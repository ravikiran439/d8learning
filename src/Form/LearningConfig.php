<?php
/**
 * @file
 * Contains \Drupal\d8learning\Form\LearningConfig.
 */
namespace Drupal\d8learning\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class LearningConfig extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'learning_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('learning_config.settings');

    $form['colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Colors'),
      '#default_value' => $config->get('learning_config.colors'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('learning_config.settings');
    $config->set('learning_config.colors', $form_state->getValue('colors'));
    $config->save();    

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'learning_config.settings',
    ];
  }
}
